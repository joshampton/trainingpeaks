This application does the basic work of parsing a fit file, using the Dynastream C# SDK, to capture the power channel as an offset from start.

1) Write an efficient method that finds the "best" 20 minute power effort.  "Best" as defined as highest continuous average for the given time period.

2) Find the 1, 5, 10, 15, and 20 minute "best" efforts using the method above on multiple threads: 

3) This program was purposely written without best practices in mind. Please refactor to parse additional channels like heart rate and speed using OO pratices and a cleaner approach.

4) Bonus:  See if you can find the subtle bug in this program.