﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace PeakswareTest.Test
{
    [TestClass]
    public class DurationResultTests
    {
        [TestMethod]
        public void Constructor()
        {
            var time = TimeSpan.FromSeconds(1);
            var durations = new List<Duration<string>> 
            {
                new Duration<string>(0, 1)
            };
            var durationResult = new DurationResult<string>(time, durations, "watts");

            Assert.AreEqual("watts", durationResult.Units);
            Assert.AreEqual(time, durationResult.TargetDuration);
            Assert.AreEqual(durations, durationResult.SelectedDurations);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_ZeroTime()
        {
            var time = TimeSpan.FromMilliseconds(0);
            var durationResult = new DurationResult<string>(time, null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullDurations()
        {
            var time = TimeSpan.FromMilliseconds(1);
            var durationResult = new DurationResult<string>(time, null, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NullUnits()
        {
            var time = TimeSpan.FromSeconds(1);
            var durations = new List<Duration<string>> 
            {
                new Duration<string>(0, 1)
            };
            var durationResult = new DurationResult<string>(time, durations, null);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_EmptyUnits()
        {
            var time = TimeSpan.FromSeconds(1);
            var durations = new List<Duration<string>> 
            {
                new Duration<string>(0, 1)
            };
            var durationResult = new DurationResult<string>(time, durations, string.Empty);
        }
    }
}
