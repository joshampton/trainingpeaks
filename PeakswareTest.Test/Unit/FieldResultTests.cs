﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeakswareTest.Test
{
    [TestClass]
    public class FitReaderResultTests
    {
        [TestMethod]
        public void Constructor()
        {
            var start = DateTime.Parse("4/4/2014");
            var fields = Enumerable.Empty<Field>();

            var result = new FitReaderResult(start, fields);

            Assert.AreEqual(start, result.Start);
            Assert.AreEqual(fields, result.Fields);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_DefaultStart()
        {
            var start = default(DateTime);
            var fields = Enumerable.Empty<Field>();

            var result = new FitReaderResult(start, fields);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullFields()
        {
            var start = DateTime.Parse("4/4/2014");

            var result = new FitReaderResult(start, null);
        }
    }
}
