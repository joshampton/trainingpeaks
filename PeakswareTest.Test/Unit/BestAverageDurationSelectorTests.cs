﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;

namespace PeakswareTest.Test
{
    [TestClass]
    public class BestAverageDurationSelectorTests
    {
        [TestMethod]
        public void Selects_Correct_Duration_1()
        {
            var unit = "watts";
            var name = "Power";
            var sampleRate = TimeSpan.FromMilliseconds(1);
            var start = DateTime.Parse("4/4/2014");

            var fields = new List<Field> 
            { 
                new Field(name, unit, 0, (ushort)1),
                new Field(name, unit, 1, (ushort)1),
                new Field(name, unit, 2, (ushort)1),
                new Field(name, unit, 3, (ushort)2),
                new Field(name, unit, 4, (ushort)2),
                new Field(name, unit, 5, (ushort)2),
            };

            var selector = new BestAverageDurationSelector();
            var results = selector.Select(fields, start, TimeSpan.FromMilliseconds(3));

            Assert.IsNotNull(results);
            Assert.AreEqual(1, results.SelectedDurations.Count());

            var result = results.SelectedDurations.Single();

            Assert.AreEqual(2, result.Value);
            Assert.AreEqual(3, result.Fields.Count);
        }

        [TestMethod]
        public void Selects_Correct_Duration_2()
        {
            var unit = "watts";
            var name = "Power";
            var sampleRate = TimeSpan.FromMilliseconds(1);
            var start = DateTime.Parse("4/4/2014");

            var fields = new List<Field> 
            { 
                new Field(name, unit, 0, (ushort)1),
                new Field(name, unit, 1, (ushort)1),
                new Field(name, unit, 2, (ushort)2),
                new Field(name, unit, 3, (ushort)2)
            };

            var selector = new BestAverageDurationSelector();

            var results = selector.Select(fields, start, TimeSpan.FromMilliseconds(2));

            Assert.IsNotNull(results);
            Assert.AreEqual(1, results.SelectedDurations.Count());

            var result = results.SelectedDurations.First();

            Assert.AreEqual(2, result.Value);
            Assert.AreEqual(2, result.Fields.Count());
        }

        [TestMethod]
        public void Selects_Correct_Durations()
        {
            var unit = "watts";
            var name = "Power";
            var sampleRate = TimeSpan.FromMilliseconds(1);
            var start = DateTime.Parse("4/4/2014");

            var fields = new List<Field> 
            { 
                new Field(name, unit, 0, (ushort)1),
                new Field(name, unit, 2, (ushort)1),
                new Field(name, unit, 4, (ushort)2),
                new Field(name, unit, 8, (ushort)2)
            };

            var selector = new BestAverageDurationSelector();

            var results = selector.Select(fields, start, TimeSpan.FromMilliseconds(2));

            Assert.IsNotNull(results);
            Assert.AreEqual(2, results.SelectedDurations.Count());

            foreach (var duration in results.SelectedDurations)
            {
                Assert.AreEqual(2, duration.Value);
                Assert.AreEqual(1, duration.Fields.Count);
            }
        }
    }
}
