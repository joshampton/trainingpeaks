﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PeakswareTest.Test
{
    [TestClass]
    public class FitReaderTests
    {
        [TestMethod]
        public void IsValid_NullStream()
        {
            var reader = new FitReader();
            Assert.IsFalse(reader.IsValid(null));
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Read_NullStream()
        {
            string fieldName = "Power";

            var reader = new FitReader();
            reader.Read(null, fieldName);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Read_NullFieldName()
        {
            var reader = new FitReader();

            using (var stream = File.Open(@"..\..\..\files\2012-05-31-11-17-12.fit", FileMode.Open))
            {
                reader.Read(stream, null);
            }
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Read_EmptyFieldName()
        {
            var reader = new FitReader();

            using (var stream = File.Open(@"..\..\..\files\2012-05-31-11-17-12.fit", FileMode.Open))
            {
                reader.Read(stream, string.Empty);
            }
        }
    }
}
