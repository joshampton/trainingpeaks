﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeakswareTest.Test
{
    [TestClass]
    public class DurationTests
    {
        [TestMethod]
        public void Constructor()
        {
            var start = 0;
            var end = 1;

            var duration = new Duration<string>(start, end);

            duration.Fields.Add(new Field("Power", "watts", 0, 10));

            Assert.AreEqual(start, duration.Start);
            Assert.AreEqual(end, duration.End);
            Assert.AreEqual("Power", duration.Name);
            Assert.AreEqual("watts", duration.Units);
        }

        [TestMethod]
        public void DerivedProperties()
        {
            var duration = new Duration<string>(0, 1);
            duration.Fields.Add(new Field("Power", "watts", 0, 10));

            Assert.AreEqual("Power", duration.Name);
            Assert.AreEqual("watts", duration.Units);
        }

        [TestMethod]
        public void DerivedProperties_NoFields()
        {
            var duration = new Duration<string>(0, 1);

            Assert.IsNull(duration.Name);
            Assert.IsNull(duration.Units);
        }
    }
}
