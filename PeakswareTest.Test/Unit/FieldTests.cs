﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeakswareTest.Test
{
    [TestClass]
    public class FieldTests
    {
        [TestMethod]
        public void Constructor()
        {
            string name = "name";
            string units = "watts";
            long offset = 1;
            object value = "value";

            var field = new Field(name, units, offset, value);

            Assert.AreEqual(name, field.Name);
            Assert.AreEqual(units, field.Units);
            Assert.AreEqual(offset, field.Offset);
            Assert.AreEqual(value, field.Value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NullName()
        {
            string units = "watts";
            object value = "value";

            var field = new Field(null, units, 0, value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_EmptyName()
        {
            string units = "watts";
            object value = "value";

            var field = new Field(string.Empty, units, 0, value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_NullUnits()
        {
            string name = "name";
            object value = "value";

            var field = new Field(name, null, 0, value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentException))]
        public void Constructor_EmptyUnits()
        {
            string name = "name";
            object value = "value";

            var field = new Field(name, string.Empty, 0, value);
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Constructor_NullValue()
        {
            string name = "name";
            string units = "watts";

            var field = new Field(name, units, 0, null);
        }
    }
}
