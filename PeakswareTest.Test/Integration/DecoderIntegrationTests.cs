﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PeakswareTest.Test
{
    [TestClass]
    public class DecoderIntegrationTests
    {
        [TestMethod]
        public void ParseValid()
        {
            List<Field> nullValues = new List<Field>(2);
            
            var decoder = new FitReader();

            var path = @"..\..\..\files\2012-05-31-11-17-12.fit";
            using(var stream = File.Open(path, FileMode.Open))
            {
                Assert.IsTrue(decoder.IsValid(stream));

                var result = decoder.Read(stream, "Power");

                Assert.AreEqual(DateTime.Parse("5/31/2012 5:17:13 PM"), result.Start);
                Assert.AreEqual(5008, result.Fields.Count());
            }
        }
    }
}
