﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace PeakswareTest
{
    public class BestAverageDurationSelector : DurationSelector<double>
    {
        protected override double Transform(IEnumerable<Field> durationFields)
        {
            return durationFields.Average(f => (int)((ushort)f.Value));
        }

        protected override IEnumerable<Duration<double>> Selector(IEnumerable<Duration<double>> durations)
        {
            return durations
                .GroupBy(p => p.Value)
                .OrderByDescending(g => g.Key)
                .FirstOrDefault() ?? Enumerable.Empty<Duration<double>>();
        }
    }
}
