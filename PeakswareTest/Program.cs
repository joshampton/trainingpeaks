﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Fit = Dynastream.Fit;
using System.IO;
using System.Threading.Tasks;
using System.Collections.Concurrent;
using System.Diagnostics;

namespace PeakswareTest
{
    class Program
    {
        static void Main(string[] args)
        {
            string path = args[0];

            FitReaderResult fieldResult = null;
            bool isValid = false;
            StringBuilder message = new StringBuilder();

            var fieldName = "Power";
            var fitReader = new FitReader();
            using (var stream = File.Open(path, FileMode.Open))
            {
                isValid = fitReader.IsValid(stream);
                if (isValid)
                    fieldResult = fitReader.Read(stream, fieldName);
            }

            if (!isValid)
            {
                message.AppendLine("Invalid File!");
            }
            else
            {
                var bestAverageSelector = new BestAverageDurationSelector();

                var tasks = new int[] { 1, 5, 10, 15, 20 }
                    .Select(i => TimeSpan.FromMinutes(i))
                    .Select(i => new Task<DurationResult<double>>(() => bestAverageSelector.Select(fieldResult.Fields, fieldResult.Start, i)))
                    .ToArray();

                foreach (var task in tasks)
                    task.Start();

                Task.WaitAll(tasks);

                foreach (var result in tasks.Select(t => t.Result))
                {
                    message.AppendFormat("Best {0} Minute {1} Effort(s):{2}", result.TargetDuration.Minutes, result.Units, Environment.NewLine);
                    foreach (var duration in result.SelectedDurations)
                        message.AppendFormat("\t{0:F2} {1} starting at {2}{3}", duration.Value, result.Units, (fieldResult.Start + TimeSpan.FromMilliseconds(duration.Start)), Environment.NewLine);
                }
            }

            Console.WriteLine(message.ToString());
        }
    }
}