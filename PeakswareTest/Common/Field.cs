﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PeakswareTest
{
    [DebuggerDisplay("Name: {Name}, Offset: {Offset}")]
    public sealed class Field
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly string name;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly string units;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly double offset;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly object value;

        public string Name { get { return this.name; } }
        public string Units { get { return this.units; } }
        public double Offset { get { return this.offset; } }
        public object Value { get { return this.value; } }

        public Field(string name, string units, double offset, object value)
        {
            if (string.IsNullOrEmpty(name))
                throw new ArgumentException("name cannot be null or empty", "name");

            if (string.IsNullOrEmpty(units))
                throw new ArgumentException("units cannot be null or empty", "units");

            if (value == null)
                throw new ArgumentNullException("value");

            this.name = name;
            this.units = units;
            this.offset = offset;
            this.value = value;
        }
    }
}
