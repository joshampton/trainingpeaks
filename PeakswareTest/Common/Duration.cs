﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PeakswareTest
{
    [DebuggerDisplay("Start: {Start}, End: {End}")]
    public class Duration<T>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly double start;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly double end;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly List<Field> fields;

        public string Name
        {
            get
            {
                return this.fields.Count > 0 ? this.fields[0].Name : null;
            }
        }

        public string Units
        {
            get
            {
                return this.fields.Count > 0 ? this.fields[0].Units : null;
            }
        }

        public double Start { get { return this.start; } }
        public double End { get { return this.end; } }
        public List<Field> Fields { get { return this.fields; } }
        public T Value { get; set; }

        public Duration(double start, double end)
        {
            this.start = start;
            this.end = end;
            this.fields = new List<Field>();
        }
    }
}