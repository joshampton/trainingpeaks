﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;

namespace PeakswareTest
{
    public abstract class DurationSelector<T>
    {
        protected abstract T Transform(IEnumerable<Field> fields);

        protected abstract IEnumerable<Duration<T>> Selector(IEnumerable<Duration<T>> durations);

        public DurationResult<T> Select(IEnumerable<Field> fields, DateTime start, TimeSpan duration)
        {
            if (duration == default(TimeSpan))
                throw new ArgumentException("duration must have non-default value", "duration");

            var durations = new List<Duration<T>>();

            var currentDuration = new Duration<T>(0, duration.TotalMilliseconds - 1);

            durations.Add(currentDuration);

            string fieldName = null;
            string unit = null;

            foreach (var field in fields.OrderBy(f => f.Offset))
            {
                if (fieldName == null && unit == null)
                {
                    fieldName = field.Name;
                    unit = field.Units;
                }

                if (field.Offset > currentDuration.End)
                {
                    currentDuration.Value = Transform(currentDuration.Fields);
                    currentDuration = new Duration<T>(currentDuration.End + 1, currentDuration.End + duration.TotalMilliseconds);
                    durations.Add(currentDuration);
                }

                currentDuration.Fields.Add(field);
            }

            currentDuration.Value = Transform(currentDuration.Fields);

            var selectedDurations = this.Selector(durations);

            return new DurationResult<T>(duration, selectedDurations, unit);
        }
    }
}
