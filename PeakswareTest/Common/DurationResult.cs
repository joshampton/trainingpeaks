﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PeakswareTest
{
    [DebuggerDisplay("TargetDuration: {TargetDuration}, Units: {Units}")]
    public class DurationResult<T>
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly TimeSpan targetDuration;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly IEnumerable<Duration<T>> selectedDurations;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly string units;

        public TimeSpan TargetDuration { get { return this.targetDuration; } }
        public IEnumerable<Duration<T>> SelectedDurations { get { return this.selectedDurations; } }
        public string Units { get { return this.units; } }

        public DurationResult(TimeSpan targetDuration, IEnumerable<Duration<T>> selectedDurations, string units)
        {
            if (targetDuration.TotalMilliseconds == 0)
                throw new ArgumentException("targetDuration must be greater than 1 millisecond", "targetDuration");

            if (selectedDurations == null)
                throw new ArgumentNullException("selectedDurations");

            if (string.IsNullOrEmpty(units))
                throw new ArgumentException("units cannot be null or empty", "units");

            this.targetDuration = targetDuration;
            this.selectedDurations = selectedDurations;
            this.units = units;
        }
    }
}
