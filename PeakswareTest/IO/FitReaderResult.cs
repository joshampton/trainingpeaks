﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace PeakswareTest
{
    [DebuggerDisplay("Start: {Start}")]
    public sealed class FitReaderResult
    {
        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly DateTime start;

        [DebuggerBrowsable(DebuggerBrowsableState.Never)]
        private readonly IEnumerable<Field> fields;

        public DateTime Start { get { return this.start; } }

        public IEnumerable<Field> Fields { get { return this.fields; } }

        public FitReaderResult(DateTime start, IEnumerable<Field> fields)
        {
            if (start == default(DateTime))
                throw new ArgumentException("start must have non-default value", "start");

            if (fields == null)
                throw new ArgumentNullException("fields");

            this.start = start;
            this.fields = fields;
        }
    }
}
