﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fit = Dynastream.Fit;

namespace PeakswareTest
{
    public sealed class FitReader : IFitReader
    {
        private readonly Fit.Decode decoder;
        private readonly Fit.MesgBroadcaster broadcaster;

        private readonly List<Field> fields;
        private string fieldName;
        private DateTime start;

        public FitReader()
        {
            this.decoder = new Fit.Decode();
            this.broadcaster = new Fit.MesgBroadcaster();

            this.decoder.MesgEvent += this.broadcaster.OnMesg;
            this.broadcaster.FileIdMesgEvent += FileEvent;
            this.broadcaster.RecordMesgEvent += RecordEvent;

            this.fields = new List<Field>();
        }

        private void RecordEvent(object sender, Fit.MesgEventArgs e)
        {
            var recordMessage = e.mesg as Fit.RecordMesg;
            if (recordMessage == null)
                return;

            var tempField = recordMessage.GetField(this.fieldName);
            if (tempField == null)
                return;
            
            var date = recordMessage.GetTimestamp().GetDateTime();
            var offset = date.Subtract(start).TotalMilliseconds;
            var field = new Field(tempField.Name, tempField.Units, offset, tempField.GetValue());

            this.fields.Add(field);
        }

        private void FileEvent(object sender, Fit.MesgEventArgs e)
        {
            var fileMessage = e.mesg as Fit.FileIdMesg;
            if (fileMessage == null)
                return;

            var date = fileMessage.GetTimeCreated().GetDateTime();

            this.start = date;
        }

        public bool IsValid(Stream stream)
        {
            return stream != null && this.decoder.IsFIT(stream) && this.decoder.CheckIntegrity(stream);
        }

        public FitReaderResult Read(Stream stream, string fieldName)
        {
            if (stream == null)
                throw new ArgumentNullException("stream");

            if (!this.IsValid(stream))
                throw new ArgumentException("invalid stream", "stream");

            if (string.IsNullOrEmpty(fieldName))
                throw new ArgumentException("fieldName cannot be null or empty");

            this.fieldName = fieldName;
            this.start = default(DateTime);
            this.fields.Clear();

            this.decoder.Read(stream);

            return new FitReaderResult(start, this.fields);
        }
    }
}
