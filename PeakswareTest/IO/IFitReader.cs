﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using Fit = Dynastream.Fit;

namespace PeakswareTest
{
    public interface IFitReader
    {
        bool IsValid(Stream stream);

        FitReaderResult Read(Stream stream, string fieldName);
    }
}
